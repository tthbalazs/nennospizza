# My solution for the Nenno's Pizza application

Cocoapods is required to build the project correctly.

- The checkout method currently cannot reach the server, thus the actual payload might be faulty.
- The cart is not persisted between app launches
- There were some assets missing from the provided archive, so I improvised some (plus sign, delete icon, add icon)
