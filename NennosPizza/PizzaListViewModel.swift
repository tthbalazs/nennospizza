//
//  PizzaListViewModel.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 09..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class PizzaListViewModel {
    static let pizzasUpdatedNotification = Notification.Name("PizzasUpdated")
    
    let dataStore: DataStore
    
    init(dataStore: DataStore) {
        self.dataStore = dataStore
        self.pizzas = dataStore.pizzas
        
        NotificationCenter.default.addObserver(self, selector: #selector(dataStoreUpdated(notification:)), name: DataStore.dataStoreUpdated, object: nil)
        
        dataStore.update(.pizzas)
    }
    
    var pizzas: [Pizza] = [] {
        didSet {
            NotificationCenter.default.post(name: PizzaListViewModel.pizzasUpdatedNotification, object: nil)
        }
    }
    
    @objc func dataStoreUpdated(notification: Notification) {
        self.pizzas = dataStore.pizzas
    }
    
    func addToCart(pizza: Pizza) {
        self.dataStore.addToCart(item: pizza)
    }
    
    // MARK: -
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pizzas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PizzaCell.identifier) as! PizzaCell
        
        let pizza = pizzas[indexPath.row]
        
        cell.configure(for: pizza)
        
        return cell
    }
    
    func pizzaDetailViewModel(for indexPath: IndexPath) -> PizzaDetailViewModel {
        let pizza = pizzas[indexPath.row]
        
        return PizzaDetailViewModel(dataStore: dataStore, pizza: pizza)
    }
}
