//
//  DrinkViewController.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 10..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class DrinkViewController: UIViewController {
    static let identifier = "DrinkViewController"
    
    @IBOutlet weak var tableView: UITableView!
    
    var drinkViewModel: DrinkViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
        
        NotificationCenter.default.addObserver(self, selector: #selector(drinksUpdated(notification:)), name: DrinkViewModel.drinksUpdatedNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addDrinkToCart(notification:)), name: DrinkCell.addDrinkToCartNotification, object: nil)
        
        guard
            let viewModel = drinkViewModel else {
                return
        }
        
        viewModel.updateDrinks()
    }
    
    // MARK: - Notifications
    
    func drinksUpdated(notification: Notification) {
        self.tableView.reloadData()
    }
    
    func addDrinkToCart(notification: Notification) {
        guard
            let userInfo = notification.userInfo,
            let drink = userInfo["drink"] as? Drink,
            let viewModel = drinkViewModel else {
                return
        }
        
        viewModel.addToCart(drink: drink)
    }
}

extension DrinkViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard
            let viewModel = drinkViewModel else {
                return 0
        }
        
        return viewModel.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard
            let viewModel = drinkViewModel else {
                return 0
        }
        
        return viewModel.tableView(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let viewModel = drinkViewModel else {
                return UITableViewCell(style: .default, reuseIdentifier: nil)
        }
        
        return viewModel.tableView(tableView, cellForRowAt: indexPath)
    }
    
}

extension DrinkViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard
            let viewModel = drinkViewModel else {
                return
        }
        
        viewModel.tableView(tableView, didSelectRowAt: indexPath)
    }
    
}
