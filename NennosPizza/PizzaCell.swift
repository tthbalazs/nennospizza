//
//  PizzaCell.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 08..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

final class PizzaCell: UITableViewCell {
    static let addPizzaToCartNotification = Notification.Name(rawValue: "AddPizzaToCartNotification")
    static let identifier = "PizzaCell"
    static let estimatedHeight: CGFloat = 178
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ingredientsLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var pizzaImageView: UIImageView!
    @IBOutlet weak var addToCartButton: UIButton!
    
    var pizza: Pizza?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pizzaImageView.image = nil
    }
    
    func configure(for pizza: Pizza) {
        self.pizza = pizza
        
        nameLabel.text = pizza.name
        ingredientsLabel.text = pizza.ingredientsLabel
        
        addToCartButton.setTitle(pizza.finalPriceLabel, for: .normal)
        
        if let url = pizza.imageURL {
            pizzaImageView.af_setImage(withURL: url)
        }
    }
    
    @IBAction func addToCart(sender: UIButton) {
        let notificationObject: [String: Any] = [
            "pizza": pizza as Any
        ]
        
        NotificationCenter.default.post(name: PizzaCell.addPizzaToCartNotification, object: nil, userInfo: notificationObject)
    }
    
}
