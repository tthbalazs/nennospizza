//
//  DrinkCell.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 10..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class DrinkCell: UITableViewCell {
    static let addDrinkToCartNotification = Notification.Name(rawValue: "AddDrinkToCartNotification")
    static let identifier = "DrinkCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var drink: Drink?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func configure(for drink: Drink) {
        self.drink = drink
        
        nameLabel.text = drink.name
        priceLabel.text = drink.finalPriceLabel
    }
    
    @IBAction func addToCart(sender: UIButton) {
        let notificationObject: [String: Any] = [
            "drink": drink as Any
        ]
        
        NotificationCenter.default.post(name: DrinkCell.addDrinkToCartNotification, object: nil, userInfo: notificationObject)
    }
}
