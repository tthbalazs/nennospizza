//
//  Drink.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 08..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation

struct Drink: CartItem {
    static let resource = Resource<Drink>(
        url: URL(string: "/get/N1mnOA_oz")!,
        parse: Drink.init,
        params: nil
    )
    
    let id: Int
    let name: String
    let price: Double?
    
    var finalPrice: Double {
        guard
            let price = price else {
                return 0
        }
        
        return price
    }
    
    var finalPriceLabel: String {
        return String(format: "$%.1f", finalPrice)
    }
    
    init?(json: JSON) {
        guard
            let id = json["id"] as? Int,
            let name = json["name"] as? String,
            let price = json["price"] else {
                return nil
        }
        
        self.id = id
        self.name = name
        
        switch price {
        case is Double:
            self.price = price as? Double
        case is String:
            self.price = Double(price as! String)
        default:
            self.price = nil
        }
    }
}
