//
//  SuccessViewController.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 10..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class SuccessViewController: UIViewController {
    static let identifier = "SuccessViewController"
    
    @IBOutlet weak var successLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        successLabel.text = "Thank you\n for your order!"
    }
    
    @IBAction func closeView(sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
