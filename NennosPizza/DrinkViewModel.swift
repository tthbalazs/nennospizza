//
//  DrinkViewModel.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 10..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class DrinkViewModel {
    static let drinksUpdatedNotification = Notification.Name("DrinksUpdated")
    
    let dataStore: DataStore
    
    var drinks: [Drink] {
        didSet {
            NotificationCenter.default.post(name: DrinkViewModel.drinksUpdatedNotification, object: nil)
        }
    }
    
    init(dataStore: DataStore) {
        self.dataStore = dataStore
        self.drinks = dataStore.drinks
        
        NotificationCenter.default.addObserver(self, selector: #selector(dataStoreUpdated(notification:)), name: DataStore.dataStoreUpdated, object: nil)
    }
    
    @objc func dataStoreUpdated(notification: Notification) {
        self.drinks = self.dataStore.drinks
    }
    
    func updateDrinks() {
        self.dataStore.update(.drinks)
    }
    
    func addToCart(drink: Drink) {
        self.dataStore.addToCart(item: drink)
    }
    
    // MARK: -
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return drinks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DrinkCell.identifier, for: indexPath) as! DrinkCell
        let drink = drinks[indexPath.row]
        
        cell.configure(for: drink)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
