//
//  CartItem.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 09..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation

protocol CartItem {
    var name: String { get }
    var finalPrice: Double { get }
    var finalPriceLabel: String { get }
}
