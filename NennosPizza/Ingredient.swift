//
//  Ingredient.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 08..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation

struct Ingredient: CartItem {
    static let resource = Resource<Ingredient>(
        url: URL(string: "/get/EkTFDCdsG")!,
        parse: Ingredient.init,
        params: nil
    )
    
    let id: Int
    let name: String
    let price: Double?
    
    var finalPrice: Double {
        guard
            let price = price else {
                return 0
        }
        
        return price
    }
    
    var finalPriceLabel: String {
        return String(format: "$%.1f", finalPrice)
    }
    
    init?(json: JSON) {
        guard
            let id = json["id"] as? Int,
            let name = json["name"] as? String,
            let price = json["price"] else {
                return nil
        }
        
        self.id = id
        self.name = name
        
        switch price {
        case is Double:
            self.price = price as? Double
        case is String:
            self.price = Double(price as! String)
        default:
            self.price = nil
        }
    }
}

extension Ingredient: Equatable {
    static func ==(lhs: Ingredient, rhs: Ingredient) -> Bool {
        return lhs.id == rhs.id
    }
}
