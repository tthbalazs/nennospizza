//
//  IngredientsHeaderCell.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 09..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class IngredientsHeaderCell: UITableViewCell {
    static let identifier = "IngredientsHeaderCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
