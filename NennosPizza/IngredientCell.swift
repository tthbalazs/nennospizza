//
//  IngredientCell.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 09..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class IngredientCell: UITableViewCell {
    static let identifier = "IngredientCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var selectedButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func configure(for ingredient: Ingredient, contains: Bool) {
        nameLabel.text = ingredient.name
        priceLabel.text = ingredient.finalPriceLabel
        
        selectedButton.isHidden = contains ? false : true
    }
}
