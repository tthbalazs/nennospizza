//
//  CartViewController.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 10..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class CartViewController: UIViewController {
    static let identifier = "CartViewController"
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var checkoutButton: UIButton!
    
    var cartViewModel: CartViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
        
        NotificationCenter.default.addObserver(self, selector: #selector(cartUpdated(notification:)), name: CartViewModel.cartUpdatedNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeItemFromCart(notification:)), name: CartCell.removeItemFromCartNotification, object: nil)
    }
    
    func cartUpdated(notification: Notification) {
        self.tableView.reloadData()
    }
    
    func removeItemFromCart(notification: Notification) {
        guard
            let userInfo = notification.userInfo,
            let item = userInfo["item"] else {
                return
        }
        
        switch item {
        case is Pizza:
            cartViewModel?.dataStore.removeFromCart(item: item as! Pizza)
        case is Drink:
            cartViewModel?.dataStore.removeFromCart(item: item as! Drink)
        default:
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard
            let identifier = segue.identifier else {
                return
        }
        
        switch identifier {
        case "ShowDrinksSegue":
            if let destination = segue.destination as? DrinkViewController {
                destination.drinkViewModel = DrinkViewModel(dataStore: DataStore.shared)
            }
        default:
            break
        }
    }
    
    @IBAction func checkout(sender: UIButton) {
        guard
            let viewController = storyboard?.instantiateViewController(withIdentifier: SuccessViewController.identifier) as? SuccessViewController else {
                return
        }
        
        cartViewModel?.checkout()
        
        self.present(viewController, animated: false) {
            self.cartViewModel?.dataStore.clearCart()
            self.navigationController?.popToRootViewController(animated: false)
        }
    }
}

extension CartViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard
            let viewModel = cartViewModel else {
                return 0
        }
        
        return viewModel.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard
            let viewModel = cartViewModel else {
                return 0
        }
        
        return viewModel.tableView(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let viewModel = cartViewModel else {
                return UITableViewCell(style: .default, reuseIdentifier: nil)
        }
        
        return viewModel.tableView(tableView, cellForRowAt: indexPath)
    }
    
}

extension CartViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard
            let viewModel = cartViewModel else {
                return
        }
        
        viewModel.tableView(tableView, didSelectRowAt: indexPath)
    }
    
}
