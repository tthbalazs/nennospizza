//
//  APIClient.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 08..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import Alamofire

final class APIClient {
    
    static let baseURL = URL(string: "http://beta.json-generator.com/api/json")!
    
    // The pizza endpoint need special attention because the basePrice is outside of the pizza object
    func loadPizzas(loaded: @escaping ([Pizza]) -> ()) {
        let url = APIClient.baseURL + Pizza.resource.url
        
        Alamofire.request(url).responseJSON { response in
            guard
                let json = response.result.value as? JSON,
                let pizzasJSON = json["pizzas"] as? [JSON],
                let basePrice = json["basePrice"] else {
                    debugPrint(response)
                    return
            }
            
            let result = pizzasJSON.flatMap({ (json) -> Pizza? in
                var price: Double = 0
                
                switch basePrice {
                case is Int:
                    price = Double(basePrice as! Int)
                case is Double:
                    price = basePrice as? Double ?? 0
                case is String:
                    price = Double(basePrice as! String) ?? 0
                default:
                    break
                }
                
                return Pizza(json: json, basePrice: price)
            })
            
            loaded(result)
        }
    }
    
    func loadMultiple<A>(of resource: Resource<A>, loaded: @escaping ([A]) -> ()) {
        let url = APIClient.baseURL + resource.url
        
        Alamofire.request(url).responseJSON { response in
            guard
                let json = response.result.value as? [JSON] else {
                    debugPrint(response)
                    return
            }
            
            let result = json.flatMap(resource.parse)
            
            loaded(result)
        }
    }
    
    func loadResource<A>(_ resource: Resource<A>, loaded: @escaping (A?) -> ()) {
        let url = APIClient.baseURL + resource.url
        
        Alamofire.request(url).responseJSON { response in
            guard
                let json = response.result.value as? JSON else {
                    debugPrint(response)
                    return
            }
            
            let result = resource.parse(json)
            
            loaded(result)
        }
    }
    
    func createResource<A>(_ resource: Resource<A>, created: @escaping (A?) -> ()) {
        let url = resource.url
        
        Alamofire.request(url, method: HTTPMethod.post, parameters: resource.params ?? nil).responseJSON { (response) in
            guard
                let json = response.result.value as? JSON else {
                    debugPrint(response)
                    return
            }
            
            let result = resource.parse(json)
            
            created(result)
        }
    }

}
