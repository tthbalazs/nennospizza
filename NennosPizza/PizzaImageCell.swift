//
//  PizzaImageCell.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 09..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class PizzaImageCell: UITableViewCell {
    static let identifier = "PizzaImageCell"
    
    @IBOutlet weak var pizzaImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pizzaImageView.image = nil
    }
    
    func configure(for pizza: Pizza) {
        if let url = pizza.imageURL {
            pizzaImageView.af_setImage(withURL: url)
        }
    }
}
