//
//  CartViewModel.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 10..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class CartViewModel {
    static let cartUpdatedNotification = Notification.Name("CartUpdated")
    
    let dataStore: DataStore
    
    var cartItems: [CartItem] {
        didSet {
            NotificationCenter.default.post(name: CartViewModel.cartUpdatedNotification, object: nil)
        }
    }
    
    init(dataStore: DataStore) {
        self.dataStore = dataStore
        self.cartItems = dataStore.cart
        
        NotificationCenter.default.addObserver(self, selector: #selector(dataStoreCartUpdated(notification:)), name: DataStore.cartUpdatedNotification, object: nil)
    }
    
    @objc func dataStoreCartUpdated(notification: Notification) {
        self.cartItems = self.dataStore.cart
    }
    
    func checkout() {
        self.dataStore.checkout()
    }
    
    // MARK: -
    
    var totalPriceLabel: String {
        return ""
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return 1
        default:
            return cartItems.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 1:
            // Total
            let cell = tableView.dequeueReusableCell(withIdentifier: CartTotalCell.identifier, for: indexPath) as! CartTotalCell
            let total = cartItems.reduce(0, { (result, item) -> Double in
                return result + item.finalPrice
            })
            
            cell.configure(for: total)
            
            return cell
        default:
            // Cart Items
            let cell = tableView.dequeueReusableCell(withIdentifier: CartCell.identifier, for: indexPath) as! CartCell
            let cartItem = cartItems[indexPath.row]
            
            cell.configure(for: cartItem)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
