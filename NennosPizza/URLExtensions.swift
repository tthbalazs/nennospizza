//
//  URLExtensions.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 08..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation

extension URL {
    static func +(lhs: URL, rhs: URL) -> URL {
        return lhs.appendingPathComponent(rhs.absoluteString)
    }
}
