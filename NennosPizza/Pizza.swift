//
//  Pizza.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 08..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation

struct Pizza: CartItem {
    static let resource = Resource<Pizza>(
        url: URL(string: "/get/NybelGcjz")!,
        parse: Pizza.init,
        params: nil
    )
    
    let imageURL: URL?
    let ingredientsList: [Int]
    let name: String
    let basePrice: Double
    let ingredients: [Ingredient]
    
    var finalPrice: Double {
        let finalPrice = basePrice + ingredients.reduce(0, { (result, ingredient) -> Double in
            return result + ingredient.finalPrice
        })
        
        return finalPrice
    }
    
    var finalPriceLabel: String {
        return String(format: "$%.1f", finalPrice)
    }
    
    var ingredientsLabel: String {
        return ingredients.reduce("", { (result, ingredient) -> String in
            if result == "" {
                return ingredient.name
            } else {
                return result + ", \(ingredient.name.lowercased())"
            }
        })
    }
    
    init?(json: JSON) {
        return nil
    }
    
    init?(json: JSON, basePrice: Double) {
        guard
            let imageURLString = json["imageUrl"] as? String,
            let ingredientsList = json["ingredients"] as? [Int],
            let name = json["name"] as? String else {
                return nil
        }
        
        self.imageURL = URL(string: imageURLString)
        self.ingredientsList = ingredientsList
        self.name = name
        self.basePrice = basePrice
        self.ingredients = []
    }
    
    init(name: String, basePrice: Double, imageURL: URL?, ingredientsList: [Int], ingredients: [Ingredient]) {
        self.name = name
        self.basePrice = basePrice
        self.imageURL = imageURL
        self.ingredientsList = ingredientsList
        self.ingredients = ingredients
    }
    
    // MARK: -
    
    func hasIngredient(_ ingredient: Int) -> Bool {
        return self.ingredientsList.contains(ingredient)
    }
    
    func hasIngredient(_ ingredient: Ingredient) -> Bool {
        return self.ingredients.contains(ingredient)
    }
    
    func addOrRemoveIngredient(_ ingredient: Ingredient) -> Pizza {
        var newIngredients: [Ingredient]
        
        if ingredients.contains(ingredient) {
            newIngredients = ingredients.filter({ ing -> Bool in
                return ing != ingredient
            })
        } else {
            newIngredients = ingredients + [ingredient]
        }
        
        return Pizza(name: self.name, basePrice: self.basePrice, imageURL: self.imageURL, ingredientsList: self.ingredientsList, ingredients: newIngredients)
    }
    
    func updateIngredients(ingredients: [Ingredient]) -> Pizza {
        var pizzaIngredients: [Ingredient] = []
        
        ingredients.forEach { ingredient in
            if self.ingredientsList.contains(ingredient.id) {
                pizzaIngredients.append(ingredient)
            }
        }
        
        return Pizza(name: self.name, basePrice: self.basePrice, imageURL: self.imageURL, ingredientsList: self.ingredientsList, ingredients: pizzaIngredients)
    }
    
    func asJSON() -> JSON {
        return [
            "name": self.name,
            "ingredients": self.ingredientsList
        ]
    }
}
