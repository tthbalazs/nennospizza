//
//  PizzaDetailViewModel.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 09..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class PizzaDetailViewModel {
    static let ingredientsUpdatedNotification = Notification.Name("IngredientsUpdated")
    
    let dataStore: DataStore
    let ingredients: [Ingredient]
    
    var pizza: Pizza?
    
    init(dataStore: DataStore, pizza: Pizza?) {
        self.dataStore = dataStore
        self.ingredients = dataStore.ingredients
        self.pizza = pizza
    }
    
    // MARK: -
    
    var titleForNavigationBar: String {
        if let pizza = self.pizza {
            return pizza.name.uppercased()
        } else {
            return "CREATE A PIZZA"
        }
    }
    
    var totalPriceLabel: String {
        guard
            let pizza = pizza else {
                return "$0"
        }
        
        return pizza.finalPriceLabel
    }
    
    func addPizzaToCart() {
        guard let pizza = self.pizza else {
            return
        }
        
        self.dataStore.addToCart(item: pizza)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        default:
            return ingredients.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            // Pizza Image
            let cell = tableView.dequeueReusableCell(withIdentifier: PizzaImageCell.identifier, for: indexPath) as! PizzaImageCell
            
            if let pizza = pizza {
                cell.configure(for: pizza)
            }
            
            return cell
        case 1:
            // Header
            let cell = tableView.dequeueReusableCell(withIdentifier: IngredientsHeaderCell.identifier, for: indexPath) as! IngredientsHeaderCell
            return cell
        default:
            // Ingredients
            let cell = tableView.dequeueReusableCell(withIdentifier: IngredientCell.identifier, for: indexPath) as! IngredientCell
            let ingredient = ingredients[indexPath.row]
            
            cell.configure(for: ingredient, contains: pizza?.hasIngredient(ingredient) ?? false)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let ingredient = ingredients[indexPath.row]
        
        if let pizza = self.pizza {
            self.pizza = pizza.addOrRemoveIngredient(ingredient)
        } else {
            self.pizza = Pizza(name: "Custom", basePrice: dataStore.basePrice, imageURL: nil, ingredientsList: [ingredient.id], ingredients: [ingredient])
        }
        
        NotificationCenter.default.post(name: PizzaDetailViewModel.ingredientsUpdatedNotification, object: nil)
    }
}
