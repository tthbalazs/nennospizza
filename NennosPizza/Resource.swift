//
//  Resource.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 08..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation

typealias JSON = [String: Any?]

struct Resource<A> {
    let url: URL
    let parse: (JSON) -> A?
    let params: JSON?
}
