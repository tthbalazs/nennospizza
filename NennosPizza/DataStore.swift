//
//  DataStore.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 09..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import BPStatusBarAlert

final class DataStore {
    static let dataStoreUpdated = Notification.Name("DataStoreUpdated")
    static let cartUpdatedNotification = Notification.Name("DataStoreCartUpdated")
    static let shared = DataStore(api: APIClient())
    
    enum Task {
        case pizzas
        case ingredients
        case drinks
    }
    
    let api: APIClient
    
    var cart: [CartItem] = [] {
        didSet {
            NotificationCenter.default.post(name: DataStore.cartUpdatedNotification, object: nil)
        }
    }
    
    var ingredients: [Ingredient] = [] {
        didSet {
            postUpdatedNotification()
        }
    }
    
    var pizzas: [Pizza] = [] {
        didSet {
            postUpdatedNotification()
        }
    }
    
    var drinks: [Drink] = [] {
        didSet {
            postUpdatedNotification()
        }
    }
    
    var basePrice: Double {
        guard
            let pizza = pizzas.first else {
                return 0
        }
        
        return pizza.basePrice
    }
    
    // MARK: -
    
    init(api: APIClient) {
        self.api = api
    }
    
    // MARK: - Cart methods
    
    func addToCart(item: CartItem) {
        self.cart.append(item)
        
        BPStatusBarAlert(duration: 0.3, delay: 3, position: .statusBar)
            .message(message: "ADDED TO CART")
            .messageColor(color: .white)
            .bgColor(color: UIColor(red: 225/255, green: 77/255, blue: 69/255, alpha: 1.0))
            .show()
    }
    
    func removeFromCart(at index: Array<CartItem>.Index) {
        self.cart.remove(at: index)
    }
    
    func removeFromCart(item: CartItem) {
        let index = self.cart.index { (cItem) -> Bool in
            return item.name == cItem.name && item.finalPrice == cItem.finalPrice
        }
        
        if index != nil {
            self.cart.remove(at: index!)
        }
    }
    
    func clearCart() {
        self.cart = []
    }
    
    func checkout() {
        var pizzas: [Pizza] = []
        var drinks: [Drink] = []
        
        cart.forEach { (cartItem) in
            switch cartItem {
            case is Pizza:
                pizzas.append(cartItem as! Pizza)
            case is Drink:
                drinks.append(cartItem as! Drink)
            default:
                break
            }
        }
        
        let order = Order(pizzas: pizzas, drinks: drinks)
        
        self.api.createResource(order.resource) { (result) in
            // No connection to server?
        }
    }
    
    // MARK: -
    
    func postUpdatedNotification() {
        NotificationCenter.default.post(name: DataStore.dataStoreUpdated, object: nil)
    }
    
    func update(_ task: Task) {
        switch task {
        case .pizzas:
            // For pizzas to display properly we also need to load the ingredients list
            self.api.loadMultiple(of: Ingredient.resource, loaded: { [weak self] ingredients in
                self?.ingredients = ingredients
                
                self?.api.loadPizzas(loaded: { [weak self] pizzas in
                    let updatedPizzas = pizzas.map({ (pizza) -> Pizza in
                        return pizza.updateIngredients(ingredients: ingredients)
                    })
                    
                    self?.pizzas = updatedPizzas
                })
            })
        case .ingredients:
            self.api.loadMultiple(of: Ingredient.resource, loaded: { [weak self] ingredients in
                self?.ingredients = ingredients
            })
        case .drinks:
            self.api.loadMultiple(of: Drink.resource, loaded: { [weak self] drinks in
                self?.drinks = drinks
            })
        }
    }
}
