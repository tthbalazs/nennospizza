//
//  CartButton.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 08..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
final class CartButton: UIButton {
    
    static let cartImage = UIImage(named: "ic_cart_button")?.withRenderingMode(.alwaysTemplate)
    static let bgColor = UIColor(red: 255/255, green: 205/255, blue: 43/255, alpha: 1)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setImage(CartButton.cartImage, for: .normal)
        
        self.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 4)
        
        self.tintColor = UIColor.white
        self.backgroundColor = CartButton.bgColor
        
        self.layer.cornerRadius = 4
        
        self.clipsToBounds = true
    }
}
