//
//  Order.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 08..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation

struct Order {
    let pizzas: [Pizza]
    let drinks: [Int]
    
    var resource: Resource<Order> {
        return Resource<Order>(
            url: URL(string: "http://posttestserver.com/post.php")!,
            parse: Order.init,
            params: self.params
        )
    }
    
    init?(json: JSON) {
        return nil
    }
    
    init(pizzas: [Pizza], drinks: [Drink]) {
        self.pizzas = pizzas
        self.drinks = drinks.flatMap({ return $0.id })
    }
    
    var params: JSON {
        return [
            "pizzas": self.pizzas.flatMap({ $0.asJSON() }),
            "drinks": self.drinks
        ]
    }
}
