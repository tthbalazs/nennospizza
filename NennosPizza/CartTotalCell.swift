//
//  CartTotalCell.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 10..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class CartTotalCell: UITableViewCell {
    static let identifier = "CartTotalCell"
    
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func configure(for price: Double) {
        priceLabel.text = String(format: "$%.1f", price)
    }
}
