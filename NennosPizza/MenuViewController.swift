//
//  MenuViewController.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 08..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class MenuViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var pizzaListViewModel: PizzaListViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = PizzaCell.estimatedHeight
        
        NotificationCenter.default.addObserver(self, selector: #selector(pizzasUpdated(notification:)), name: PizzaListViewModel.pizzasUpdatedNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(addPizzaToCart(notification:)), name: PizzaCell.addPizzaToCartNotification, object: nil)
        
        self.pizzaListViewModel = PizzaListViewModel(dataStore: DataStore.shared)
    }
    
    // MARK: -
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard
            let identifier = segue.identifier else {
                return
        }
        
        switch identifier {
        case "CreatePizzaSegue":
            if let destination = segue.destination as? PizzaDetailViewController {
                destination.pizzaDetailViewModel = PizzaDetailViewModel(dataStore: DataStore.shared, pizza: nil)
            }
        case "ShowCartSegue":
            if let destination = segue.destination as? CartViewController {
                destination.cartViewModel = CartViewModel(dataStore: DataStore.shared)
            }
        default:
            break
        }
    }
    
    // MARK: - Notifications
    
    func addPizzaToCart(notification: Notification) {
        guard
            let userInfo = notification.userInfo,
            let pizza = userInfo["pizza"] as? Pizza,
            let viewModel = pizzaListViewModel else {
                return
        }
        
        viewModel.addToCart(pizza: pizza)
    }
    
    func pizzasUpdated(notification: Notification) {
        self.tableView.reloadData()
    }
}

extension MenuViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let viewModel = pizzaListViewModel else {
            return 0
        }
        
        return viewModel.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = pizzaListViewModel else {
            return 0
        }
        
        return viewModel.tableView(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = pizzaListViewModel else {
            return UITableViewCell(style: .default, reuseIdentifier: nil)
        }
        
        return viewModel.tableView(tableView, cellForRowAt: indexPath)
    }
}

extension MenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewModel = pizzaListViewModel else {
            return
        }
        
        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: PizzaDetailViewController.identifier) as? PizzaDetailViewController {
            viewController.pizzaDetailViewModel = viewModel.pizzaDetailViewModel(for: indexPath)
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
