//
//  CartCell.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 09..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class CartCell: UITableViewCell {
    static let removeItemFromCartNotification = Notification.Name(rawValue: "RemoveItemFromCartNotification")
    static let identifier = "CartCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    var item: CartItem?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func configure(for cartItem: CartItem) {
        self.item = cartItem
        
        nameLabel.text = cartItem.name
        priceLabel.text = cartItem.finalPriceLabel
    }
    
    @IBAction func removeItemFromCart() {
        let notificationObject: [String: Any] = [
            "item": item as Any
        ]
        
        NotificationCenter.default.post(name: CartCell.removeItemFromCartNotification, object: nil, userInfo: notificationObject)
    }
}
