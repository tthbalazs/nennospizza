//
//  PizzaDetailViewController.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 09..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import Foundation
import UIKit

final class PizzaDetailViewController: UIViewController {
    static let identifier = "PizzaDetailViewController"
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cartButton: UIButton!
    
    var pizzaDetailViewModel: PizzaDetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
        
        NotificationCenter.default.addObserver(self, selector: #selector(ingredientsUpdated(notification:)), name: PizzaDetailViewModel.ingredientsUpdatedNotification, object: nil)
        
        guard
            let viewModel = pizzaDetailViewModel else {
                return
        }
        
        self.navigationItem.title = viewModel.titleForNavigationBar
        
        updateCartButtonTitle(with: viewModel.totalPriceLabel)
    }
    
    func ingredientsUpdated(notification: Notification) {
        guard
            let viewModel = pizzaDetailViewModel else {
                return
        }
        
        self.tableView.reloadData()
        
        updateCartButtonTitle(with: viewModel.totalPriceLabel)
    }
    
    func updateCartButtonTitle(with price: String) {
        self.cartButton.setTitle("ADD TO CART (\(price))", for: .normal)
    }
    
    // MARK: - IBActions
    
    @IBAction func addToCart(sender: UIButton) {
        guard
            let viewModel = pizzaDetailViewModel else {
                return
        }
        
        viewModel.addPizzaToCart()
    }
}

extension PizzaDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard
            let viewModel = pizzaDetailViewModel else {
                return 0
        }
        
        return viewModel.numberOfSections(in: tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard
            let viewModel = pizzaDetailViewModel else {
                return 0
        }
        
        return viewModel.tableView(tableView, numberOfRowsInSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let viewModel = pizzaDetailViewModel else {
                return UITableViewCell(style: .default, reuseIdentifier: nil)
        }
        
        return viewModel.tableView(tableView, cellForRowAt: indexPath)
    }
    
}

extension PizzaDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard
            let viewModel = pizzaDetailViewModel else {
                return
        }
        
        viewModel.tableView(tableView, didSelectRowAt: indexPath)
    }
    
}
