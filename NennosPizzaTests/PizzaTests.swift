//
//  PizzaTests.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 08..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import XCTest
@testable import NennosPizza

class PizzaTests: XCTestCase {
    
    let basePrice: Double = 4
    let json: JSON = [
        "imageUrl": "https://cdn.pbrd.co/images/tOhJQ5N3.png",
        "ingredients": [
            1,
            2,
            3,
            4
        ],
        "name": "Boscaiola"
    ]
    
    let ingredientJSON: JSON = [
        "id": 1,
        "name": "Mozzarella",
        "price": 1.2
    ]
    
    let url = URL(string: "http://beta.json-generator.com/api/json/get/NybelGcjz")
    
    func testInit() {
        guard
            let pizza = Pizza.init(json: json, basePrice: basePrice) else {
                return XCTFail()
        }
        
        XCTAssertEqual(pizza.imageURL, URL(string: json["imageUrl"] as! String))
        XCTAssertEqual(pizza.ingredientsList, json["ingredients"] as! [Int])
        XCTAssertEqual(pizza.name, json["name"] as! String)
        XCTAssertEqual(pizza.basePrice, basePrice)
    }
    
    func testResource() {
        let resource = Pizza.resource
        
        XCTAssertEqual(APIClient.baseURL + resource.url, url)
    }
    
    func testAddIngredient() {
        let missingIngredientJSON: JSON = [
            "id": 6,
            "name": "Mozzarella",
            "price": 1.2
        ]
        
        guard
            let pizza = Pizza.init(json: json, basePrice: basePrice),
            let ingredient = Ingredient(json: missingIngredientJSON) else {
                return XCTFail()
        }
        
        XCTAssertFalse(pizza.hasIngredient(ingredient.id))
        
        let newPizza = pizza.addOrRemoveIngredient(ingredient)
        
        XCTAssertTrue(newPizza.hasIngredient(ingredient))
    }
    
    func testHasIngredient() {
        guard
            let pizza = Pizza.init(json: json, basePrice: basePrice),
            let ingredient = Ingredient(json: ingredientJSON) else {
                return XCTFail()
        }
        
        XCTAssertTrue(pizza.hasIngredient(ingredient.id))
        
        let newPizza = pizza.addOrRemoveIngredient(ingredient)
        
        XCTAssertTrue(newPizza.hasIngredient(ingredient))
    }
    
}
