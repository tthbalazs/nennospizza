//
//  DrinkTests.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 08..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import XCTest
@testable import NennosPizza

class DrinkTests: XCTestCase {
    
    let doubleJSON: JSON = [
        "id": 3,
        "name": "Coke",
        "price": 2.5
    ]
    
    let stringJSON: JSON = [
        "id": 3,
        "name": "Coke",
        "price": "2.5"
    ]
    
    let url = URL(string: "http://beta.json-generator.com/api/json/get/N1mnOA_oz")
    
    func testDoubleInit() {
        guard
            let drink = Drink.init(json: doubleJSON) else {
                return XCTFail()
        }
        
        XCTAssertEqual(drink.id, doubleJSON["id"] as? Int)
        XCTAssertEqual(drink.name, doubleJSON["name"] as? String)
        XCTAssertEqual(drink.price, doubleJSON["price"] as? Double)
    }
    
    func testStringInit() {
        guard
            let drink = Drink.init(json: stringJSON) else {
                return XCTFail()
        }
        
        XCTAssertEqual(drink.id, stringJSON["id"] as? Int)
        XCTAssertEqual(drink.name, stringJSON["name"] as? String)
        XCTAssertEqual(drink.price, Double(stringJSON["price"] as! String))
    }
    
    func testResource() {
        let resource = Drink.resource
        
        XCTAssertEqual(APIClient.baseURL + resource.url, url)
    }
    
    func testPriceLabel() {
        guard
            let drink = Drink.init(json: doubleJSON) else {
                return XCTFail()
        }
        
        XCTAssertEqual(drink.finalPriceLabel, String(format: "$%.1f", 2.5))
    }
    
}
