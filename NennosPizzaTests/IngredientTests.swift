//
//  IngredientTests.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 08..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import XCTest
@testable import NennosPizza

class IngredientTests: XCTestCase {
    
    let doubleJSON: JSON = [
        "id": 1,
        "name": "Mozzarella",
        "price": 1.2
    ]
    
    let stringJSON: JSON = [
        "id": 1,
        "name": "Mozzarella",
        "price": "1.0"
    ]
    
    let url = URL(string: "http://beta.json-generator.com/api/json/get/EkTFDCdsG")
    
    func testDoubleInit() {
        guard
            let ingredient = Ingredient.init(json: doubleJSON) else {
                return XCTFail()
        }
        
        XCTAssertEqual(ingredient.id, doubleJSON["id"] as? Int)
        XCTAssertEqual(ingredient.name, doubleJSON["name"] as? String)
        XCTAssertEqual(ingredient.price, doubleJSON["price"] as? Double)
    }
    
    func testStringInit() {
        guard
            let ingredient = Ingredient.init(json: stringJSON) else {
                return XCTFail()
        }
        
        XCTAssertEqual(ingredient.id, stringJSON["id"] as? Int)
        XCTAssertEqual(ingredient.name, stringJSON["name"] as? String)
        XCTAssertEqual(ingredient.price, Double(stringJSON["price"] as! String))
    }
    
    func testResource() {
        let resource = Ingredient.resource
        
        XCTAssertEqual(APIClient.baseURL + resource.url, url)
    }
    
    func testPriceLabel() {
        guard
            let ingredient = Ingredient.init(json: doubleJSON) else {
                return XCTFail()
        }
        
        XCTAssertEqual(ingredient.finalPriceLabel, String(format: "$%.1f", 1.2))
    }
}
