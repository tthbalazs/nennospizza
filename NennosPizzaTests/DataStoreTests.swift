//
//  DataStoreTests.swift
//  NennosPizza
//
//  Created by Tóth Balázs on 2017. 05. 10..
//  Copyright © 2017. Tóth Balázs. All rights reserved.
//

import XCTest
@testable import NennosPizza

class DataStoreTests: XCTestCase {
    
    let dataStore = DataStore.shared
    
    let drinkJSON: JSON = [
        "id": 3,
        "name": "Coke",
        "price": 2.5
    ]
    
    func testAddToCart() {
        guard
            let drink = Drink(json: drinkJSON) else {
                return XCTFail()
        }
        
        XCTAssertTrue(dataStore.cart.count == 0)
        
        dataStore.addToCart(item: drink)
        
        XCTAssertTrue(dataStore.cart.contains(where: { (item) -> Bool in
            return item.name == drink.name
        }))
    }
    
    func testRemoveFromCart() {
        guard
            let drink = Drink(json: drinkJSON) else {
                return XCTFail()
        }
        
        dataStore.cart = [drink]
        
        XCTAssertTrue(dataStore.cart.count == 1)
        
        dataStore.removeFromCart(item: drink)
        
        XCTAssertTrue(dataStore.cart.count == 0)
    }
    
}
